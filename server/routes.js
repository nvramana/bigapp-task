'use strict';

module.exports = function(app){
	//insert roots here
	app.use('/api/users',require('./api/user'));
	app.use('/api/balanced', require('./api/userbalance'));
};