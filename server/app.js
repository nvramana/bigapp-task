//run javascript code in strict mode
'use strict';

//set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var express = require('express');
var mongoose = require('mongoose');
var config = require('./config/environment');

//connect to database

mongoose.connect(config.mongo.uri,{useNewUrlParser : true});


//setup server
var app = express();
var server = require('http').createServer(app);

require('./config/express')(app);
require('./routes')(app);

server.listen(config.port,config.ip,function(){
	console.log('express server listening on %d, in %s mode',config.port,app.get('env'))
})