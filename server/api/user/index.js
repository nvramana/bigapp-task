'use strict';
var express = require('express');
var controller = require('./controller');
var User = require('./model.js');
var router = express.Router();
var auth = require('../../auth/authservice');

/**
 * List of services in user collection.
 */
router.post('/signup', controller.signup);
router.post('/login', controller.login);
router.get('/getallusers', auth.isAuthenticated(), controller.getAllUsers);
router.post('/deleteuser', auth.isAuthenticated(), controller.deleteUser);

//Exporting the router.
module.exports = router;

