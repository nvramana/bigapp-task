var User = require('./model');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var handleError = function(res, err) {
  return res.status(422).json(err);
};

/**
 * Here is signup service for creating the user.
 */
exports.signup = function(req, res){
	User.create(req.body, (err, user) =>{
		if(err){return handleError(res, err)}
			if(user){
				res.send("successfully created the user");
			}
	})
}

/**
 * Below is the login service for user.
 */

exports.login = function(req, res){
	User.findOne({username: req.body.username, password: req.body.password},function(err, user){
		if(err){return handleError(res, err)}
			if(user){
				let token = jwt.sign({_id: user._id}, config.secrets.session);
				res.send({token: token, message : "success"});
			}
	})
}

/**
 * Service to get the all users (only accessible for admin role).
 */

exports.getAllUsers = function(req, res){
	User.findOne({_id: req.user._id, role: "admin"},function(err, admin){
		if(err){return handleError(res,err)}
		if(admin){
			User.find({},function(err, allUsers){
				if(err){return handleError(res,err)}
				if(allUsers.length > 0){
					res.send(allUsers);
				}else{
					res.send('No user registered till date');
				}
			})
		}else{
			res.send("Sorry, Admin only access the all user data.")
		}
	})
}

/**
 * service to remove the user 
 */
exports.deleteUser = function(req, res){
	User.findOne({_id: req.user._id, role: "admin"},function(err, admin){
		if(err){return handleError(res, err)}
		if(admin){
			User.deleteOne({username: req.body.username, role: "user"},function(err, status){
				if(err){return handleError(res, err)}
				res.send('user removed successfully');
			})
		}else{
			res.send('Admin only remove the user..');
		}
	})
}