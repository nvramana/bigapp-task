//Mongoose is used to create the schema model.
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var UserSchema = new Schema({
	username : {
		type : String,
		required : true,
		unique: true
	},
	password : {
		type : String,
		required : true
	},
	email:{
		type:String,
		lowercase:true,
		unique: true
	},
	DOB : {
		type : String
	},
	role: {
		type: String,
		default: "user"
	}
	
});


module.exports = mongoose.model('User',UserSchema);