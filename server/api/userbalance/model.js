var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var BalanceSchema = new Schema({
	username: {
		type : String
	},
	message:{
		type: String
	},
	attempts : {
		type: Number,
		default: 0
	}
	
});


module.exports = mongoose.model('UserBalance',BalanceSchema);