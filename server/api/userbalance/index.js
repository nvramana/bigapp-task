var express = require('express');
var controller = require('./controller.js');
var UserBalance = require('./model');
var User = require('../user/model.js');
var auth = require('../../auth/authservice');

var router = express.Router();
router.post('/', auth.isAuthenticated(), controller.isInputBalanced);

module.exports = router;



