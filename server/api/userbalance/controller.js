var UserBalance = require('../userbalance/model');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var User = require('../user/model');

var handleError = function(res, err) {
  return res.status(422).json(err);
};

/**
 * The below service is used for the purpose of finding parenthesis are balanced or unbalanced.
 */
exports.isInputBalanced = function(req, res){
	User.findOne({_id: req.user._id},function(err, user){
		if(err){return handleError(res, err)}
			if(user){
				UserBalance.findOne({username : user.username},function(err, userBalance){
					if(err){return handleError(res, err)}
						let resObj = {};
						let msg = isBalanced(req.body.parenthesis);
						resObj.username = user.username;
						resObj.message = msg;
						resObj.attempts = (userBalance) ? userBalance.attempts + 1 : 1 ;						
						if(userBalance){
							UserBalance.updateOne({username: user.username},{$set:resObj},function(err, updated){
								if(updated){
									UserBalance.findOne({username: user.username},function(err, doc){
										if(err){return handleError(res, err)}
											res.send(doc);
									})
								}
							})
						}else{
							UserBalance.create(resObj, function(err, objt){
								if(err){return handleError(res, err)}
									if(objt){
										res.send(objt);
									}
							})
						}
				})
				
			}
	})
}

/**
 * herper function for finding the input parenthesis is balanced or not.
 * @param {*} str 
 */

function isBalanced(str){
	let len = str.length - 1;
	let result;
	if(str.length % 2 !== 0){
		result = "failed";
	}else{
		for(let i = 0; i < str.length; i++){
			if(i > len - i){
				result = "success";
				break;
			}
			if(((str.charCodeAt(i)) === (str.charCodeAt(len - i) - 2))
			|| ((str.charCodeAt(i)) === (str.charCodeAt(len - i) - 1))){
				continue;
			}else{
				result = "failed";
				break;
			}
		}
	}
	return result;
}

