'use strict';
var _=require('lodash');

//all configurations will extend these options
var all = {
	env: process.env.NODE_ENV,

	//server port
	port:process.env.PORT || 9000,

	//Server ip

	ip:process.env.IP || '0.0.0.0',

	secrets:{
		session:'bigapp-secret'
	}


}

module.exports = _.merge(
all,
require('./'+process.env.NODE_ENV + '.js') || {}
	)
