

var config = require('../config/environment');
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
var compose = require('composable-middleware');
var validateJwt = expressJwt({ secret: config.secrets.session });
var User = require('../api/user/model');


//Helper function to authenticate the user.
function isAuthenticated() {
  
    return compose()
      // Validate jwt
      .use(function(req, res, next) {
        // allow access_token to be passed through query parameter as well
        if(req.query && req.query.hasOwnProperty('access_token')) {
         req.headers.authorization = 'Bearer ' + req.query.access_token;
        }
        validateJwt(req, res, next);
      })
      // Attach user to request
      .use(function(req, res, next) {
       User.findOne({_id : req.user._id}, function (err, user) {
         if (err) return next(err);
          if (!user) return res.status(401).send('Unauthorized');
          req.user = user;
          next();
        });
      });
  }

  exports.isAuthenticated = isAuthenticated;
  