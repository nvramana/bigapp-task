Required Softwares:
===================================

--> Node js
--> Mongodb



How to run?
==========================

--> go to the root directory and run the command "node app.js".



Below i mentioned the list of services which i have created in this task:
==========================================================================


1) (post) http://localhost:9000/api/users/signup

example request:

{ username:ram, email:ram@gmail.com, password:ram123, role:admin, DOB:07/05/1993 }



2) (post) http://localhost:9000/api/users/login

example request:

{ username:ram, password:ram123 }



3) (post) http://localhost:9000/api/balanced

example request:
	{ parenthesis : ({[]})  }
The above service is post service. Here we need to provide the JWT token through postman authentication because this service is authenticated with JWT token.



4) (get) http://localhost:9000/api/users/getallusers

The above service is get service, we don't require body. Here we need to provide the admin role JWT token through postman authentication because this service is authenticated with JWT token.



5) (post) http://localhost:9000/api/users/deleteuser

example request:

{ username:ramana }

note:- Here we need to provide admin role JWT token as well as username of the user which will want to delete. 